﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Feeds.Views.Home
{
    /// <summary>
    /// Class for getting rss data from url.
    /// </summary>
    public class RssGetData : RssVariables
    {
        //Datasource
        private string RssUrl = "https://flipboard.com/@raimoseero/feed-nii8kd0sz?rss";

        /// <summary>
        /// Get rss data from url
        /// </summary>
        /// <returns></returns>
        public List<RssVariables> GetData()
        {
            XDocument xdoc = XDocument.Load(RssUrl);

            List<RssVariables> ElementLists = (from item in xdoc.Descendants(Variables[0])
                                               select new RssVariables
                                               {
                                                   Variable1 = ((string)item.Element(Variables[1])).RemoveEntity() ?? String.Empty,
                                                   Variable2 = ((string)item.Element(Variables[2])) ?? String.Empty,

                                                   Variable3 = ((string)item.Element(XmlnsMedia + Variables[9])) != null ?
                                                   ((string)item.Element(XmlnsMedia + Variables[9]).Attribute(Variables[3])) : String.Empty,

                                                   Variable4 = ((string)item.Element(Variables[4])).RemoveEntity() ?? String.Empty,
                                                   Variable5 = ((string)item.Element(Variables[4])) != null ?
                                                   ((string)item.Element(Variables[4]).Attribute(Variables[5])) : String.Empty,

                                                   Variable6 = ((string)item.Element(Variables[6])).RemoveEntity() ?? String.Empty,
                                                   Variable7 = ((string)item.Element(Variables[7])).RemoveEntity() ?? String.Empty,
                                                   Variable8 = ((string)item.Element(Variables[8])).RemoveEntity() ?? String.Empty
                                               }).ToList();
            return ElementLists;
        }
    }
}


